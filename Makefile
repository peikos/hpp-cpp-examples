CC = g++
FLAGS = -Wall

all:
	make -C 0_autoverhuur
	make -C 1_gameshop

clean:
	make -C 0_autoverhuur clean
	make -C 1_gameshop clean

run:
	make -C 0_autoverhuur run
	make -C 1_gameshop run
