#include <iostream>
#include <set>
#include <vector>

typedef std::pair<int, int> point;
typedef std::pair<point, point> line;
typedef std::set<point> pointSet;

inline constexpr int SIGN(int x) { return (x > 0) - (x < 0); }

// Return the signed distance between a point and a line.
int signed_dist(line l, point p) {
  point p1 = l.first;
  point p2 = l.second;

  return (p.second - p1.second) * (p2.first - p1.first) -
         (p2.second - p1.second) * (p.first - p1.first);
}

// Side can have value 1 or -1 specifying each of the parts made by the line L
void convex_hull(pointSet& h, std::vector<point>& points, line l, int side) {
  int ind = -1;
  int max_dist = 0;

  // Find furthert point on this side
  for (unsigned int i = 0; i < points.size(); ++i) {
    int temp = abs(signed_dist(l, points[i]));
    if (SIGN(signed_dist(l, points[i])) == side && temp > max_dist) {
      ind = i;
      max_dist = temp;
    }
  }

  // If no point is found, end points of L are part of the convex hull.
  if (ind == -1) {
    h.insert(l.first);
    h.insert(l.second);
    return;
  }

  // Create two new lines
  line l1 = {points[ind], l.first};
  line l2 = {points[ind], l.second};

  // Recurse
  convex_hull(h, points, l1, -SIGN(signed_dist(l1, l.second)));
  convex_hull(h, points, l2, -SIGN(signed_dist(l2, l.first)));
}

void convex_hull(pointSet& h, std::vector<point>& points) {
  // Split space based on line between x-extreme points

  int min_x = 0, max_x = 0;
  for (unsigned int i = 1; i < points.size(); ++i) {
    if (points[i].first < points[min_x].first)
      min_x = i;
    if (points[i].first > points[max_x].first)
      max_x = i;
  }

  line l = {points[min_x], points[max_x]};

  // Initialise recursion on top half
  convex_hull(h, points, l, 1);

  // Initialise recursion on top half
  convex_hull(h, points, l, -1);
}

int main() {
  pointSet h;

  std::vector<point> points = {{0, 3}, {1, 1}, {2, 2}, {4, 4},
      {0, 0}, {1, 2}, {3, 1}, {3, 3}};

  convex_hull(h, points);

  std::cout << "Convex Hull:";
  for (auto p : h) {
    std::cout << "(" << p.first << ", " << p.second << ") ";
  }

  std::cout << std::endl;

  return 0;
}
