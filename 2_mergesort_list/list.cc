#include "list.hh"

using std::move;
using std::unique_ptr, std::make_unique;
using std::initializer_list;
using std::ostream;
using std::views::drop, std::views::reverse;
using std::invalid_argument;

/// Constructors

// Build by prepending, comparable to Haskell's (:) operator.
// Node `tail` is passed by rvalue, reference, guaranteeing
// the node hasn't been bound elsewhere making the unique_ptr
// unique. O(1) complexity.
List::List(int head, List&& tail) : head(head) {
  this->len = tail.len + 1;
  this->next = make_unique<List>(move(tail));
}

// Same operation, but take a unique_ptr instead of a list node value.
// This will help facilitate the initializer_list constructor as it
// allows us to use move semantics on previously generated nodes.
List::List(int& head, unique_ptr<List> tail) : head(head) {
  this->len = tail->len + 1;
  this->next = move(tail);
}

// For convenience: build a list from an initialiser list.
// Allows syntax as such:
//
// List foo = {1,2,3};
//
// This function is slightly convoluted, as we build a recursive structure
// from a non-recursive one. Both the first and last element are treated
// separately, the first being initialised as the const `this` pointer in
// the constructor. The last item has no `next` item and is constructor
// separately as well.
// initializer_list allows iteration (for range loop) and provides a pointer
// to the first item's memory address (array.begin()) which is dereferenced
// using * to get the first value. The array.end() function returns the address
// past the last address, so we adjust the pointer by -1 and dereference after.
List::List(initializer_list<int> array) : head(*array.begin()) {
  unique_ptr<List> tail = make_unique<List>(List(*(array.end()-1)));

  for (auto i : reverse(array | drop(1)) | drop(1)) {
    tail = make_unique<List>(List(i, move(tail)));
  }

  this->len = tail->len + 1;
  this->next = move(tail);
}

// Append an item to the back of the list. O(n) complexity.
void List::append(int new_item) {
  unique_ptr<List> new_node = make_unique<List>(List(new_item));

  // Case for appending to list of length > 1
  if (this->next) {
    unique_ptr<List>* cursor = &this->next; // Mutable pointer to unique_ptr.
                                            // Iteration by adjusting cursor
                                            // to next item repeatedly.
                                            // Starts at second item as this
                                            // is the first behind a unique_ptr.
    (*cursor)->len += 1;
    while ((*cursor)->next) {               // Keep iterating until end
      cursor = &(*cursor)->next;
      (*cursor)->len += 1;                  // ... updating len along the way
    }
    (*cursor)->next = move(new_node);       // Move new node into position
  } else { // Case for list of length == 1  // Append to `this`
    this->next = move(new_node);
  }

  this->len += 1;
}

/// Getters and Setters

int List::length() {
  return this->len;
}

/// Mergesort

// Split a list by returning the last `n` items. Returned items are removed from
// the original list-instance, which is truncated to length - n. Return new list
// via unique_ptr.

unique_ptr<List> List::split(unsigned int n) {
  // Check if removing too much
  if (n >= this->len) {
    throw invalid_argument("Cannot split more than the entire list");
  }

  // Remove nothing: keep original, return empty pointer.
  if(n == 0) {
    return nullptr;
  }

  // If we need to keep a single item, we skip iteration again.
  if (n == this->len-1) {
    unique_ptr<List> rest = move(this->next);
    this->next = nullptr;

    rest->len = n;
    this->len = this->len - n;

    return rest;
  }

  // General case: use an iterating pointer and traverse to split-point:
  unique_ptr<List>* cursor = &this->next;

  for (unsigned int i = 1; i < this->len-n-1; ++i) {
    (*cursor)->len -= n;
    cursor = &(*cursor)->next;
  }

  (*cursor)->len -= n;
  unique_ptr<List> rest = move((*cursor)->next); // Here, move tail part
                                                 // to new entry point.

  // Due to move semantics, we don't need to truncate the original. All that
  // was returned is no longer accessible from the original location.

  rest->len = n;
  this->len -= n;

  return rest;
}

// Merge two sorted lists into a single sorted list.
unique_ptr<List> List::merge(unique_ptr<List> l, unique_ptr<List> r) {

  unique_ptr<List> sorted;   // Pointer to start of sorted list (returned)
  unique_ptr<List>* cursor;  // Mutable pointer for iteration

  int len = l->len + r->len; // Calculate length before references change

  if (l->head < r->head) {   // Pick smallest item as head for sorted list
    sorted = move(l);
    l = move(sorted->next);
  } else {
    sorted = move(r);
    r = move(sorted->next);
  }

  sorted->len = len;
  cursor = &sorted;          // Start from first item

  while (l && r) {           // Take smallest item and truncate from 2 lists
    if (l->head < r->head) {
      (*cursor)->next = move(l);       // Append list to result
      l = move((*cursor)->next->next); // Move all but first item back to `l`
                                       // (remainder of unsorted input)
    } else {
      (*cursor)->next = move(r);
      r = move((*cursor)->next->next);
    }
    (*cursor)->len = len--;
    cursor = &(*cursor)->next;         // Update cursor regardless of choice
  }

  // While loop is done, one input list is empty
  if (l) {
    (*cursor)->next = move(l);  // Move non-empty list in its entirity
  } else {
    (*cursor)->next = move(r);
  }

  while ((*cursor)->next) {     // Continue traversal to update lengths
    (*cursor)->len = len--;
    cursor = &(*cursor)->next;
  }

  return sorted;
}

// Recursive merge sort
unique_ptr<List> List::sort(unique_ptr<List> list) {

  // Base case: single item lists are sorted by definition
  if (list->len == 1) {
    return list;
  }

  // Split off half of the list
  unique_ptr<List> split_off = list->split(list->len / 2);

  unique_ptr<List> l = List::sort(move(split_off)); // First half; sort
  unique_ptr<List> r = List::sort(move(list));      // Second half; sort

  return List::merge(move(l), move(r)); // Merge both sorted lists

  // Lists are moved after splitting, into recursive calls and finally
  // to merge() using move semantics. Sorting uses linear space O(n), as
  // no copies are ever made and the original linked list requires O(n).
}

// Printing list with length using cout <<
ostream& operator<<(ostream& os, const List& l) {
  unique_ptr<List> const* cursor = &l.next;

  os << "List[" << l.len << "| ";
  os << l.head;
  while (*cursor) {
    os << " ";
    os << (*cursor)->head;
    cursor = &(*cursor)->next;
  }
  os << "]";
  return os;
}

/*
// Alternative printing via recursive calls. Yields output like (1 : (2 : 3))
ostream& operator<<(ostream& os, const List& l) {
  os << "(";
  os << l.head;
  if (l.next) {
    os << " : ";
    os << *l.next;
  }
  os << ")";
  return os;
}
*/
