#pragma once

#include <iostream>
#include <memory>    // Shared pointers
#include <stdexcept> // Exceptions
#include <ranges>    // Reverse / drop from ranges

// This class demonstrates memory management and move semantics using
// unique_ptr. Most methods are non-recursive, instead iterating using
// raw pointers to unique_ptr's. This design was chosen not because of
// performance considerations, but because it allows showcasing some
// relevant challenges when dealing with (smart) pointers.

class List {
  private:
    int head;
    unsigned int len;
    std::unique_ptr<List> next;
  public:
    List(int head) : head(head), len(1), next(nullptr) {}
    List(int head, List&& tail);
    List(int& head, std::unique_ptr<List> tail);
    List(std::initializer_list<int> array);

    void append(int new_item);

    int length();

    std::unique_ptr<List> split(unsigned int n);

    // Merge and sort as class methods, constructing new values.
    static std::unique_ptr<List> merge(
        std::unique_ptr<List> l,
        std::unique_ptr<List> r);

    static std::unique_ptr<List> sort(std::unique_ptr<List> list);

    friend std::ostream& operator<<(std::ostream& os, const List& l);
};
