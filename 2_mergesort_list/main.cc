#include "list.hh"

using std::cout, std::endl;

int main() {

  cout << endl << "[34m=== Cons Building ===[0m" << endl << endl;

  List l = List(0, List(1, List(2, List(3))));
  cout << l << endl;

  cout << endl << "[34m=== Initialisiser List ===[0m" << endl << endl;

  List l2 = {4,5,6};
  cout << l2 << endl;

  cout << endl << "[34m=== Append ===[0m" << endl << endl;

  List l3 = List(1);
  cout << l3 << endl;

  l3.append(2);
  cout << l3 << endl;

  l3.append(3);
  cout << l3 << endl;

  l3.append(4);
  cout << l3 << endl;

  cout << endl << "[34m=== Split ===[0m" << endl << endl;

  auto ten = std::make_unique<List>(
      std::initializer_list<int>({0,1,2,3,4,5,6,7,8,9}));

  cout << "Before: " << *ten << endl;

  auto rest = ten->split(7);

  cout << "Keep: " << *ten << endl;
  cout << "Return: " << *rest << endl;

  cout << endl << "[34m=== Merge ===[0m" << endl << endl;

  auto even = std::make_unique<List>(
      std::initializer_list<int>({0,2,4,6}));
  auto odd = std::make_unique<List>(
      std::initializer_list<int>({1,3,5,7}));

  cout << "Even: " << *even << endl;
  cout << "Odd: " << *odd << endl;

  auto merged = List::merge(std::move(even), std::move(odd));
  cout << "Merge: " << *merged << endl;

  cout << endl << "[34m=== Sort ===[0m" << endl << endl;

  auto tel = std::make_unique<List>(
      std::initializer_list<int>({0,6,1,4,2,1,5,5,7,2}));
  cout << "Before: " << *tel << endl;
  tel = List::sort(std::move(tel));
  cout << "Sorted: " << *tel << endl;

  return 0;
}
