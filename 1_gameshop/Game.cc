#include "Game.hh"

Game::Game(std::string name, int jaar, float nieuwprijs) : name(name), jaar(jaar), nieuwprijs(nieuwprijs) {}

float Game::getDagwaarde() const {
  time_t curr_time = time(NULL);
  tm *tm_local = localtime(&curr_time);
  int jaar = tm_local->tm_year + 1900;

  return this->nieuwprijs * pow(0.7, jaar - this->jaar);
}

// Overload de == operator, zodat we 2 games kunnen vergelijken op gelijkheid.
bool operator==(const Game &lhs, const Game &rhs) {
  return lhs.name == rhs.name;
}

std::ostream& operator<<(std::ostream& os, const Game& g) {
  os << g.name
     << ", uitgegeven in " << g.jaar
     << "; nieuwprijs: €" << g.nieuwprijs
     << " nu voor: €" << g.getDagwaarde();
  return os;
}
