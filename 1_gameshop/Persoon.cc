#include "Persoon.hh"

Persoon::Persoon(std::string name, float budget) : name(name), budget(budget) {}

bool Persoon::heeft(std::shared_ptr<const Game> g) const {
  // Linear search uit standard library, maakt gebruik van overloaded ==
  return std::find_if(
      this->games.begin(),
      this->games.end(),
      [g](std::shared_ptr<const Game> c){ return *c == *g; }
      ) != this->games.end();
}

bool Persoon::koop(std::shared_ptr<const Game> g) {
  if (g->getDagwaarde() <= budget && ! this->heeft(g)) {
    budget -= g->getDagwaarde();
    this->games.push_back(std::reference_wrapper(g));
    return true;
  } else {
    return false;
  }
}

bool Persoon::verkoop(std::shared_ptr<const Game> g, Persoon& koper) {
  if (this->heeft(g) && koper.koop(g)) {  // Hergebruik koop() - volgorde van bools is belangrijk!
                                          // && short-circuit, als this Persoon het spel niet heeft
                                          // dan wordt koop() niet geprobeerd.
    this->budget += g->getDagwaarde();
    // Remove en erase [https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom]
    // Ook hier wordt intern de == van Game gebruikt.
    auto itr = std::remove(this->games.begin(), this->games.end(), g);
    this->games.erase(itr, this->games.end());
    return true;
  } else {
    return false;
  }
}

std::ostream& operator<<(std::ostream& os, const Persoon& p) {
  os << std::fixed << std::setprecision(2) << p.name
     << " heeft een budget van €" << p.budget
     << " en bezit de volgende games:";
  for (auto g : std::as_const(p.games)) {
    os << std::endl << *g;
  }
  return os;
}
