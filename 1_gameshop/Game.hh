#pragma once

#include <ctime>
#include <iostream>
#include <math.h>
#include "../NoCopy.hh"

class Game: noncopyable {
  private:
    std::string name;
    int jaar;
    float nieuwprijs;
  public:
    Game(std::string name, int jaar, float dagprijs);
    float getDagwaarde() const;
    friend std::ostream& operator<<(std::ostream& os, const Game& g);
    friend bool operator==(const Game& lhs, const Game& rhs);
};
