#include <ctime>
#include <iostream>
#include <memory>
#include "Game.hh"
#include "Persoon.hh"

using namespace std;

int main() {
  std::time_t result = std::time(NULL);
  cout << std::asctime(std::localtime(&result));
  time_t curr_time = time(NULL);
  tm *tm_local = localtime(&curr_time);
  int releaseJaar1 = tm_local->tm_year + 1900 - 1; // 1 jaar geleden
  int releaseJaar2 = tm_local->tm_year + 1900 - 2; // 2 jaar geleden

  std::shared_ptr<const Game> g1 = std::make_shared<const Game>("Just Cause 3", releaseJaar1, 49.98);
  std::shared_ptr<const Game> g2 = std::make_shared<const Game>("Need for Speed: Rivals", releaseJaar2, 45.99);
  std::shared_ptr<const Game> g3 = std::make_shared<const Game>("Need for Speed: Rivals", releaseJaar2, 45.99);


  Persoon p1("Eric", 200);
  Persoon p2("Hans", 55);
  Persoon p3("Arno", 185);

  // Druk de volgende transacties af (pas de code aan)
  cout << "p1 koopt g1: ";
  cout << (p1.koop(g1) ? "gelukt" : "niet gelukt") << endl;
  cout << "p1 koopt g2: ";
  cout << (p1.koop(g2) ? "gelukt" : "niet gelukt") << endl;
  cout << "p1 koopt g3: ";
  cout << (p1.koop(g3) ? "gelukt" : "niet gelukt") << endl;
  cout << "p2 koopt g2: ";
  cout << (p2.koop(g2) ? "gelukt" : "niet gelukt") << endl;
  cout << "p2 koopt g1: ";
  cout << (p2.koop(g1) ? "gelukt" : "niet gelukt") << endl;
  cout << "p3 koopt g3: ";
  cout << (p3.koop(g3) ? "gelukt" : "niet gelukt") << endl << endl;

  // Druk personen p1 p2 p3 nu af naar de console
  cout << p1 << endl << endl << endl;
  cout << p2 << endl << endl << endl;
  cout << p3 << endl << endl << endl;

  // Druk de volgende transacties af (pas de code aan)
  cout << "p1 verkoopt g1 aan p3: ";
  cout << (p1.verkoop(g1, p3) ? "gelukt" : "niet gelukt") << endl;
  cout << "p2 verkoopt g2 aan p3: ";
  cout << (p2.verkoop(g2, p3) ? "gelukt" : "niet gelukt") << endl;
  cout << "p2 verkoopt g1 aan p1: ";
  cout << (p2.verkoop(g1, p1) ? "gelukt" : "niet gelukt") << endl << endl << endl;

  // Druk personen p1 p2 p3 nu af naar de console
  cout << p1 << endl << endl << endl;
  cout << p2 << endl << endl << endl;
  cout << p3 << endl;

}
