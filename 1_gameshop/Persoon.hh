#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <bits/stdc++.h>
#include "Game.hh"
#include "../NoCopy.hh"

// noncopyable is hier specifiek handig bij verkoop(); deze zal een compile-error geven als deze per
// ongeluk een persoon accepteert in plaats van een referentie - als dit niet het geval is, dan kan
// de game aan een clone van een persoon verkocht worden, waardoor run-time resultaten niet kloppen.
class Persoon: noncopyable {
  private:
    std::string name;
    float budget;
    std::vector<std::shared_ptr<const Game>> games; // Sla referenties naar spellen op.
  public:
    Persoon(std::string name, float budget);
    bool heeft(std::shared_ptr<const Game> g) const;
    bool koop(std::shared_ptr<const Game> g);
    bool verkoop(std::shared_ptr<const Game> g, Persoon& koper);
    friend std::ostream& operator<<(std::ostream& os, const Persoon& p);
};
