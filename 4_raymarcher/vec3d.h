#pragma once

#include <cmath>
#include <iostream>

class Vec3D {
  private:
    double x, y, z;

  public:
    Vec3D(double x, double y, double z) : x(x), y(y), z(z) {}
    Vec3D(std::initializer_list<double> array);
    double get_x();
    double get_y();
    double get_z();
    double norm() const;
    double dot(const Vec3D& rhs) const;
    static Vec3D unit(double x, double y, double z);

    friend Vec3D operator+(Vec3D, const Vec3D&);
    friend Vec3D operator-(Vec3D, const Vec3D&);
    friend Vec3D operator*(double, Vec3D);
    friend Vec3D operator/(Vec3D, double);
    friend std::ostream& operator<<(std::ostream& os, const Vec3D& v);
};
