#include <cmath>
#include <algorithm>
#include <ostream>
#include <iostream>
#include <mpi.h>

#define cimg_display 0        // No window plz
#include "CImg.h"

#include "vec3d.h"
#include "consts.h"
#include "frame.h"
#include "animation.h"
#include "sdf.h"

using std::cout, std::endl;

// constexpr is MACRO, but better!
//   (typed and compile-time checked, readable errors)

constexpr double PI = 3.141529;
inline constexpr double DEG2RAD(double d) { return d * PI / 180.0; }

void renderFrame(animation& frames, unsigned int t, SDF s);

const Vec3D x_tr = { 0.03, 0.0, 0.0 };

const auto sdf =
  blend(
    rotate_xz(rotate_xy(cube(1.0), 0.05), 0.025),
    ball(1.0)
  );

int main (int argc, char *argv[]) {
  int id = -1;

  /* Added for MPI */
  int numProcesses = -1;
  int length = -1;

  char myHostName[MPI_MAX_PROCESSOR_NAME];
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
  MPI_Get_processor_name (myHostName, &length);
  /* End of MPI */

  unsigned int f;

  animation frames(FRAMES);

  cout << endl << "Process " << id << " is generating frames..." << endl;


  /* # Without MPI / numprocesses:
   *
   * for (f = 0; f < FRAMES; f++) {
   *
   * # Chunking:
   *
   * int job_size = FRAMES / numProcesses;
   * int start = id * job_size;
   * int end = start + job_size;
   * for (f = start; f < end; ++f) {
   *
   * Here, each node will render a contiguous chunk. Be aware of potential 
   * rounding errors, as job_size * numProcessess may not be equal to FRAMES.
   *
   * # Striping:
   *
   * for (f = id; f < FRAMES; f += numProcesses) {
   *
   * Here, we use striping instead of chunking. Given 4 threads:
   *  - root will render 0, 4,  8, 12, ...
   *  - id 1 will render 1, 5,  9, 13, ...
   *  - id 2 will render 2, 6, 10, 14, ...
   *  - id 3 will render 3, 7, 11, 15, ...
   *
   * This is an easy way to divide work if work load is more or less balanced.
   * If data needs to be distributed, use collectives.
   * Here, `id` contains enough information.
   *
   */


  for (f = 0; f < FRAMES; ++f) {
    cout << endl << "Node " << id << ", frame " << f << endl;
    renderFrame(frames, f, sdf);
  }

  cimg_library::CImg<byte> vid(WIDTH, HEIGHT, FRAMES, 3);
  cimg_forXYZ(vid, x, y, z) {
    vid(x,y,z,RED) = frames[z].get_channel(x,y,RED);
    vid(x,y,z,GREEN) = frames[z].get_channel(x,y,GREEN);
    vid(x,y,z,BLUE) = frames[z].get_channel(x,y,BLUE);
  }
  vid.save_video("animation.avi");

  /* Added for MPI */
  MPI_Finalize();
  /* End of MPI */
}

Vec3D rayDir(double view_field, int pix_x, int pix_y) {
  double x = pix_x - WIDTH / 2.0;
  double y = pix_y - HEIGHT / 2.0;
  double z = HEIGHT / tan(DEG2RAD(view_field) / 2.0);

  return Vec3D::unit(x, y, -z);
}

double march(SDF sdf, Vec3D v_eye, Vec3D v_rayDir, double t) {
  double s_rayLen = MIN_DIST;

  for (unsigned int s = 0; s <= MAX_STEPS; s++) {
    double step = sdf(v_eye + s_rayLen * v_rayDir, t);

    if (step < EPSILON) {
      return s_rayLen;
    }

    s_rayLen += step;

    if (s_rayLen >= MAX_DIST) {
      return MAX_DIST;
    }
  }
  return MAX_DIST;
}

void renderFrame(animation& frames, unsigned int t, SDF sdf) {
  Vec3D v_eye = {0.0, 0.0, 5.0};

  // This could be done nicely using an omp collapsed for loop!
  for (unsigned x = 0; x < WIDTH; x++)
    for (unsigned y = 0; y < HEIGHT; y++) {
      Vec3D v_dir = rayDir(45.0, x, y);
      double dist = march(sdf, v_eye, v_dir, t);

      if (dist >= MAX_DIST) { // black
        frames[t].set_colour(x, y, {0, 0, 0} );
      } else {                 // Colour pixels based on distance
        byte v = byte(255.0 
               * (1.0 - std::min(pow(dist / 100.0, 0.1), pow(dist / 5.0, 3.0))));

        frames[t].set_colour(x, y, {v, 0, v} );
      }
    }
}
