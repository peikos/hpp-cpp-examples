#pragma once

// Interesting parameters
constexpr unsigned int FRAMES = 240;
constexpr unsigned int WIDTH = 400;
constexpr unsigned int HEIGHT = 300;

// Leave-as-is parameters
constexpr double MAX_STEPS = 255;
constexpr double MIN_DIST = 0.0;
constexpr double MAX_DIST = 100.0;
constexpr double EPSILON = 0.0001;
