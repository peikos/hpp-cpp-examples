#include "vec3d.h"

Vec3D::Vec3D(std::initializer_list<double> array) {
  this->x = array.begin()[0];
  this->y = array.begin()[1];
  this->z = array.begin()[2];
}

Vec3D Vec3D::unit(double x, double y, double z) {
  Vec3D v = Vec3D(x, y, z);
  return v / v.norm();
}

double Vec3D::get_x() { return this->x; }
double Vec3D::get_y() { return this->y; }
double Vec3D::get_z() { return this->z; }

double Vec3D::dot(const Vec3D& rhs) const {
  return this->x * rhs.x + this->y * rhs.y + this->z * rhs.z;
}

double Vec3D::norm() const {
  return sqrt(this->dot(*this));
}

std::ostream& operator<<(std::ostream& os, const Vec3D& v) {
  os << "<" << v.x << ", " << v.y << ", " << v.z << ">";
  return os;
}

Vec3D operator+(Vec3D lhs, const Vec3D& rhs) {
  lhs.x += rhs.x;
  lhs.y += rhs.y;
  lhs.z += rhs.z;
  return lhs;
}

Vec3D operator-(Vec3D lhs, const Vec3D& rhs) {
  lhs.x -= rhs.x;
  lhs.y -= rhs.y;
  lhs.z -= rhs.z;
  return lhs;
}

Vec3D operator*(double lhs, Vec3D rhs) {
  rhs.x *= lhs;
  rhs.y *= lhs;
  rhs.z *= lhs;
  return rhs;
}

Vec3D operator/(Vec3D lhs, double rhs) {
  lhs.x /= rhs;
  lhs.y /= rhs;
  lhs.z /= rhs;
  return lhs;
}

