#include "sdf.h"

SDF rotate_xy(std::function<double (Vec3D, double)> sdf, double speed) {
  return [sdf, speed](Vec3D p, double t){ 
    double px = p.get_x();
    double py = p.get_y();
    double pz = p.get_z();

    double tx = px;

    px = cos(t * speed) * px + sin(t * speed) * py;
    py = -sin(t * speed) * tx + cos(t * speed) * py;

    return sdf(Vec3D { px, py, pz }, t); };
}

SDF rotate_xz(std::function<double (Vec3D, double)> sdf, double speed) {
  return [sdf, speed](Vec3D p, double t){ 
    double px = p.get_x();
    double py = p.get_y();
    double pz = p.get_z();

    double tx = px;

    px = cos(t * speed) * px + sin(t * speed) * pz;
    pz = -sin(t * speed) * tx + cos(t * speed) * pz;

    return sdf(Vec3D { px, py, pz }, t); };
}

SDF rotate_yz(std::function<double (Vec3D, double)> sdf, double speed) {
  return [sdf, speed](Vec3D p, double t){ 
    double px = p.get_x();
    double py = p.get_y();
    double pz = p.get_z();

    double ty = py;

    py = cos(t * speed) * py + sin(t * speed) * pz;
    pz = -sin(t * speed) * ty + cos(t * speed) * pz;

    return sdf(Vec3D { px, py, pz }, t); };
}

SDF ball(double radius) {
  return [radius](Vec3D p, double t) { return p.norm() - radius; };
}

SDF cube(double size) {
  return [size](Vec3D p, double t) {
    double size = 0.5;
    double px = p.get_x();
    double py = p.get_y();
    double pz = p.get_z();

    double x = std::max(px - size, -px - size);
    double y = std::max(py - size, -py - size);
    double z = std::max(pz - size, -pz - size);
    return std::max(x, std::max(y, z));
  };
}

SDF blend(SDF s1, SDF s2) {
  return [s1, s2](Vec3D p, double t) {
    double a = t * 0.01;
    return a * s1(p,t) + (1-a) * s2(p,t);
  };
}
