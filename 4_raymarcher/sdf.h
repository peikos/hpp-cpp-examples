#pragma once

#include<functional>
#include <cmath>
#include "vec3d.h"

// Type aliases for SDF
typedef std::function<double (Vec3D, double)> SDF;

SDF rotate_xy(SDF sdf, double speed);

SDF rotate_xz(SDF sdf, double speed);

SDF rotate_yz(SDF sdf, double speed);

SDF ball(double radius);

SDF cube(double size);

SDF blend(SDF a, SDF b);
