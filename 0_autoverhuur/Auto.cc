#include "Auto.hh"

// Initialiser list, empty body
// - Manual assignment is duurder (eerst wordt een default value gemaakt, die
//   meteen wordt overschreven).
// - Initialiser list werkt ook als geen default value gemaakt kan worden.
Auto::Auto(std::string name, float price) : name(name), price(price) {}

// this is een geheugenadres - deze wordt opgevraagd en aangesproken met ->price
float Auto::getKosten(float days, float discount) const {
  return this->price * days * (100 - discount) / 100;
}

// Default voor printen in C++
// std::cout is een output stream verbonden aan de terminal output, << (normaliter de bitshift)
// "schuift" informatie in een output stream. Printen ziet er als volgt uit:
//
//     std::cout << "Hello World" << std::endl;
//
// (endl staat voor end line). Veel datatypes kunnen direct zonder conversie geprint worden.
// Om een datatype printbaar te maken moet je de << operator overloaden, zie hieronder.
// De operator krijgt een referentie naar een outputstream mee (hier os), en geeft een
// nieuwe referentie-outputstream terug. Als tweede argument wordt een object om te printen
// meegegeven. De methode staat als friend in de Auto class in de .hh file, wat betekent
// dat de methode niet bij de class hoort, maar wel bij alle private methods en variabelen kan.
std::ostream &operator<<(std::ostream &os, const Auto& a) {
  os << a.name << " met prijs per dag: " << std::fixed << std::setprecision(2)
     << a.price; // a is hier een value, dus direct aanspreken met .price
  return os;
}

// Niet standaard maar deel van de opdracht beschrijving (vertaald uit Java).
// Hier wordt een outputstream gemaakt die naar een string kan worden geconverteerd.
// Verder wordt de hierboven beschreven << operator gebruikt (don't repeat yourself).
std::string Auto::toString() const {
  std::stringstream ss;
  ss << *this;             // * vraagt de waarde van het adres `this` op
  return ss.str();
}
