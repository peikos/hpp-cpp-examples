#include "Klant.hh"

Klant::Klant(std::string name) : name(name) {}

double Klant::getKorting() const {
  return this->korting;
}

void Klant::setKorting(float korting) {
  this->korting = korting;
}

std::ostream& operator<<(std::ostream& os, const Klant& k) {
  os << std::fixed << std::setprecision(1)
     << k.name
     << " (korting: " << k.korting << "%)";
  return os;
}

std::string Klant::toString() const {
  std::stringstream ss;
  ss << *this;
  return ss.str();
}
