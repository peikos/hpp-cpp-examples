#include <iostream>
#include <memory>
#include "Auto.hh"
#include "AutoHuur.hh"
#include "Klant.hh"

using std::cout, std::endl; // cout en endl zijn zonder std:: (namespace identifier) te gebruiken.

// Uit opdrachtbeschrijving
int main() {
  cout << std::fixed << std::setprecision(2); // Laat floats op 2 decimalen afgerond printen

  AutoHuur ah1;
  cout << "Eerste autohuur: " << ah1 << endl;
  cout << endl;

  std::shared_ptr<Klant> k = std::make_shared<Klant>("Mijnheer de Vries");
  k->setKorting(10.0);

  ah1.setHuurder(k);
  cout << "Eerste autohuur: " << ah1 << endl;
  cout << endl;
  std::shared_ptr<Auto> a1 = std::make_shared<Auto>("Peugot 207", 50);
  ah1.setGehuurdeAuto(a1);
  ah1.setAantalDagen(4);
  cout << "Eerste autohuur:" << ah1 << endl;
  cout << endl;

  AutoHuur ah2;
  std::shared_ptr<Auto> a2 = std::make_shared<Auto>("Ferrari", 3500);
  ah2.setGehuurdeAuto(a2);
  ah2.setHuurder(k);
  ah2.setAantalDagen(1);
  cout << "Tweede autohuur: " << ah2 << endl;
  cout << endl;

  cout << "Gehuurd: " << ah1.getGehuurdeAuto() << endl;
  cout << "Gehuurd: " << ah2.getGehuurdeAuto() << endl;
}
