#pragma once

#include <iomanip>
#include <iostream>
#include <sstream>
#include "../NoCopy.hh"

class Klant: noncopyable {
  private:
    std::string name;
    float korting;
  public:
    Klant(std::string);
    double getKorting() const;
    void setKorting(float korting);
    friend std::ostream& operator<<(std::ostream& os, const Klant& k);
    std::string toString() const;
};
