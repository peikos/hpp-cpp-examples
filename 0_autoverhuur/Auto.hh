#pragma once // Nieuwe stijl (aanbevolen)

// Header file bevat de class en function declarations, maar geen implementaties.
// De declarations dienen als placeholders, de linker zoekt de juiste code erbij.
// Header files worden geimporteerd om classes en functies in scope te halen.
// Dit zorgt er ook voor dat functies in scope zijn voor hun implementatie,
// zodat functies niet op volgorde hoeven te staan en recursie mogelijk wordt.

#include <iostream>
#include <iomanip>  // Voor decimalen bij float formatting
#include <sstream>  // Voor toString()
#include "../NoCopy.hh"

// noncopyable is een optionele toevoeging. Dit dwingt ons om met referenties te werken, en geeft een
// error als er een kopie gemaakt wordt. Hier voorkomt het vooral onnodige kopien, in 1_gameshop kan
// noncopyable ook helpen om runtime fouten om te zetten in compile time errors.
class Auto: noncopyable {
  private:
    std::string name;
    float price;
  public:
    Auto(std::string name, float price); // Constructor
    float getKosten(float days, float discount) const; // const methods passen de instance niet aan.
    friend std::ostream& operator<<(std::ostream& os, const Auto& a);  // Zie implementatie in Auto.cc voor toelichting.
    std::string toString() const;
};
