#ifndef AUTO_HUUR // Oude stijl (niet aanbevolen)
#define AUTO_HUUR // Gebruik #pragma once

#include <iostream>
#include <memory>
#include "Auto.hh"
#include "Klant.hh"

class AutoHuur {
  private:
    // auto en klant opgeslagen als shared pointers.
    std::shared_ptr<Auto> a;
    std::shared_ptr<Klant> k;
    int dagen;
  public:
    AutoHuur();
    const Auto& getGehuurdeAuto() const; // Geef constante referenties terug
    const Klant& getHuurder() const;
    void setHuurder(std::shared_ptr<Klant> k); // Accepteer shared pointers
    void setGehuurdeAuto(std::shared_ptr<Auto> a);
    void setAantalDagen(int dagen);
    friend std::ostream& operator<<(std::ostream& os, const AutoHuur& ah);
    std::string toString() const;
};

#endif
