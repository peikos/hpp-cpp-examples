#include "AutoHuur.hh"

// a en k hebben een optional type (denk Maybe uit Haskell). std::nullopt maakt een Nothing aan.
AutoHuur::AutoHuur(): a( nullptr ), k( nullptr ) {
  dagen = 0;
}

void AutoHuur::setHuurder(std::shared_ptr<Klant> k) {
  this->k = std::move(k);
}

void AutoHuur::setGehuurdeAuto(std::shared_ptr<Auto> a) {
  this->a = std::move(a);
}

const Klant& AutoHuur::getHuurder() const {
  return *(this->k);
}

const Auto& AutoHuur::getGehuurdeAuto() const {
  return *(this->a);
}

void AutoHuur::setAantalDagen(int d) {
  this->dagen = d;
}

std::ostream& operator<<(std::ostream& os, const AutoHuur& ah) {
  os << std::endl
     << "  " << (ah.a ? ah.getGehuurdeAuto().toString() : "er is geen auto bekend") << std::endl
     << "  " << (ah.k ? ah.getHuurder().toString() : "er is geen huurder bekend") << std::endl
     << "  aantal dagen: " 
     << (ah.dagen ? ah.dagen : 0)
     << " en dat kost "
     << std::fixed << std::setprecision(1) // Bij print afronden op 1 decimaal
     << (ah.a ? ah.getGehuurdeAuto().getKosten(ah.dagen, ah.getHuurder().getKorting()) : 0);
     ;
  return os;
}

std::string AutoHuur::toString() const {
  std::stringstream ss;
  ss << *this;
  return ss.str();
}
